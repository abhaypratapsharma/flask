# pull official base image
FROM python:3.8.5-alpine
# set work directory
WORKDIR /app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
# install dependencies
RUN pip install --upgrade pip
RUN chmod +x .
COPY ./requirements.txt requirements.txt
RUN export LDFLAGS="-L/usr/local/opt/openssl/lib"
#RUN pip install -r requirements.txt
RUN python3 -m pip install -r ./requirements.txt

# copy project
COPY . /app

EXPOSE 5000
#RUN ls -la app/
ENTRYPOINT ["python"]
CMD ["app.py"]